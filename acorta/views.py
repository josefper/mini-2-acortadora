from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

from .models import Contenido, Recurso
from django.template import loader


# Create your views here.
def check_valor(valor):
    if not valor.startswith('http://') and not valor.startswith('https://') and valor != "":
        valor = "https://" + valor
    return valor


def show_content(request):
    contenidos = Contenido.objects.all()

    # Carga la plantilla
    template = loader.get_template('contenidos.html')
    context = {
        'contenidos': contenidos,
    }
    return HttpResponse(template.render(context, request))


def create_short():
    try:
        numero = Recurso.objects.get(identifier=0)
        short = str(numero.num)
        numero.num = numero.num + 1
        numero.save()
        return short
    except Recurso.DoesNotExist:
        numero = Recurso(identifier=0, num=1)
        numero.save()
        short = str(numero.num)
        numero.num = numero.num + 1
        numero.save()
        return short


@csrf_exempt
def get_content(request):
    try:
        if request.method == "POST":
            if ("url" in request.POST) and request.POST["url"] != "":
                valor = check_valor(request.POST["url"])
                content = Contenido.objects.get(valor=valor)
                if "short" in request.POST:
                    if request.POST["short"] != "":
                        short = request.POST["short"]
                    else:
                        short = create_short()

                    content.clave = short  # Sobreescribir el valor de la url acortada
                    content.save()

        elif request.method == "GET":
            valor = request.GET.get("url")
            if (valor is not None) and (valor != ""):
                valor = check_valor(valor)
                content = Contenido.objects.get(valor=valor)
                short = request.GET.get("short")
                if short is not None:
                    if short == "":
                        short = create_short()
                    content.clave = short  # Sobreescribir el valor de la url acortada
                    content.save()

        # Preparo la respuesta
        response = show_content(request)
        return response

    except Contenido.DoesNotExist:
        if request.method == "POST":
            if "url" in request.POST and request.POST["url"] != "":
                valor = check_valor(request.POST["url"])
                if "short" in request.POST:
                    if request.POST["short"] != "":
                        short = request.POST["short"]
                        new_content = Contenido(clave=short, valor=valor)
                        new_content.save()
                    else:
                        short = create_short()
                        new_content = Contenido(clave=short, valor=valor)
                        new_content.save()

            # Preparo la respuesta
            response = show_content(request)
            return response
        elif request.method == "GET":
            valor = request.GET.get("url")
            if (valor is not None) and (valor != ""):
                valor = check_valor(valor)
                short = request.GET.get("short")
                if short is not None:
                    if short == "":
                        short = create_short()
                    new_content = Contenido(clave=short, valor=valor)
                    new_content.save()

            # Preparo la respuesta
            response = show_content(request)
            return response
        else:
            raise Http404("No existe el contenido.")


@csrf_exempt
def redirect_to_url(request, redirection):
    try:
        content = Contenido.objects.get(clave=redirection)

        # Redirigir al usuario
        redirect_url = redirect(content.valor)
        return redirect_url
    except Contenido.DoesNotExist:
        raise Http404("No existe el contenido.")
